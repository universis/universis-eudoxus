import { DataError, TraceUtils } from '@themost/common';
import { DataObjectState } from '@themost/data';

export class ValidationResult {
	/**
	 * @param {boolean} success
	 * @param {string=} code
	 * @param {string=} message
	 * @param {string=} innerMessage
	 * @param {any=} data
	 */
	constructor(success, code, message, innerMessage, data) {
		Object.defineProperty(this, 'type', {
			value: this.constructor.name,
			writable: true,
			enumerable: true,
		});
		this.success =
			typeof success !== 'undefined' && success !== null ? success : false;
		this.statusCode = this.success ? 200 : 422;
		this.code = code;
		this.message = message;
		this.innerMessage = innerMessage;
		this.data = data;
	}
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
	return beforeSaveAsync(event)
		.then((validationResult) => {
			if (validationResult && !validationResult.success) {
				return callback(validationResult);
			}
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

/**
 * @param {DataEventArgs} event
 */
export async function beforeSaveAsync(event) {
	// operate only on update
	if (event.state !== DataObjectState.Update) {
		return;
	}
	const target = event.target;
	// operate only if the target object has classes
	if (!(Array.isArray(target.classes) && target.classes.length)) {
		return;
	}
	// and at least one is about to get deleted
	const deletedClasses = target.classes.filter(
		(courseClass) =>
			courseClass.$state === 4 &&
			(courseClass.validationResult == null ||
				courseClass.validationResult.success === true)
	);
	if (!deletedClasses.length) {
		return;
	}
	const previous = event.previous;
	if (previous == null) {
		throw new DataError(
			'E_PREVIOUS',
			'The previous state of the object cannot be determined',
			null,
			'StudentPeriodRegistration'
		);
	}
	// get student, year and period
	// all are editable: false
	const studentObject = previous.student,
		academicYear = previous.registrationYear,
		academicPeriod = previous.registrationPeriod;

	if (!(studentObject && academicYear && academicPeriod)) {
		throw new DataError(
			'E_ATTRIBUTES',
			'Student, registrationYear and registrationPeriod may not be empty at the previous state.'
		);
	}
	const context = event.model.context,
		student = context.model('Student').convert(studentObject),
		serviceSettings = context
			.getConfiguration()
			.getSourceAt('settings/universis/eudoxus'),
		translateService = context
			.getApplication()
			.getStrategy(function TranslateService() {}),
		allowResponseFailure =
			serviceSettings && serviceSettings.allowResponseFailure != null
				? serviceSettings.allowResponseFailure
				: true;

	let studentBookDeliveries;
	try {
		const data = {
			academicYear: academicYear.id || academicYear,
			academicPeriod: academicPeriod.id || academicPeriod,
		};
		// get book deliveries for student, year and period
		studentBookDeliveries = await student.getBookDeliveriesForYearAndPeriod(
			data
		);
	} catch (err) {
		// always log error
		TraceUtils.error(err);
		// if response failure is allowed
		if (allowResponseFailure) {
			// do nothing
			return;
		}
		// else, the whole registration save fails
		const validationResult = new ValidationResult(
			false,
			'EFAIL',
			translateService.translate(
				'The registration did not get updated because an error occured while reaching the EUDOXUS service.'
			)
		);
		return validationResult;
	}
	if (
		studentBookDeliveries == null ||
		(studentBookDeliveries.books && !studentBookDeliveries.books.length)
	) {
		return;
	}
	const deliveredBooksForClasses = [];
	// enumerate deleted classes
	for (const courseClass of deletedClasses) {
		if (courseClass.displayCode == null) {
			continue;
		}
		// try to find if the corresponding book has been delivered
		const deliveredBook = studentBookDeliveries.books.find(
			(book) => (book.courceCode == courseClass.displayCode && book.statementBookType === 'NORMAL')
		);
		if (!deliveredBook) {
			continue;
		}
		// and push student course class data
		deliveredBooksForClasses.push(courseClass);
	}
	if (deliveredBooksForClasses.length) {
		const validationResult = Object.assign(
			new ValidationResult(
				false,
				'EFAIL',
				translateService.translate(
					'The registration did not get updated because a book has been delivered or requested by the student for at least one of the removed courses.'
				),
				null,
				deliveredBooksForClasses
			),
			{ statusCode: 403 }
		);
		validationResult.type = 'BookDeliveryResult';
		return validationResult;
	}
	// and exit
	return;
}
