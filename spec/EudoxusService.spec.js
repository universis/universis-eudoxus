/**
 * Gets an instance of universis api server
 * Important Note: Run this test as a part of universis api server project
 * @type {any | Application}
 */
const app = require('../../../dist/server/app');
const {EudoxusService} = require("../EudoxusService");
const {StudentIdentifierMapper, StudentInstituteIdentifierMapper} = require("../studentMapper");
const {ExpressDataApplication} = require("@themost/express");
const request = require('supertest');
const path = require('path');
const pathToRegexp = require('path-to-regexp');

describe('EudoxusService', () => {
    let context;
    beforeEach((done) => {

        context = app.get(ExpressDataApplication.name).createContext();
        const regExp = pathToRegexp('/eudoxus', [], {
            "sensitive": false,
            "strict": false,
            "end": false
        });
        let findIndex = app._router.stack.findIndex((item) => {
            return item.regexp && item.regexp.toString() === regExp.toString();
        });
        if (findIndex >= 0) {
            app._router.stack.splice(findIndex, 1);
        }

        // set whitelist
        app.get(ExpressDataApplication.name).getConfiguration()
            .setSourceAt('settings/universis/eudoxus/whitelist', [
                '127.0.0.1',
                '::ffff:127.0.0.1'
            ])
        return done();
    });
    afterEach((done) => {
        if (context) {
            return context.finalize(() => {
                return done();
            });
        }
        return done();
    })
   it('should create instance', () => {
       const service = new EudoxusService(app.get(ExpressDataApplication.name));
       expect(service).toBeTruthy();
   });
   it('should GET /eudoxus/version', async () => {

       app.get(ExpressDataApplication.name).useService(EudoxusService);

       const response = await request(app)
           .get('/eudoxus/version')
           .expect(200);
           expect(response).toBeTruthy();
           expect(response.body.version).toBeTruthy();
   });
    it('should GET /eudoxus/getStudentStatusAndSemester/0/207', async () => {
        app.get(ExpressDataApplication.name).useService(EudoxusService);
        const response = await request(app)
            .get('/eudoxus/getStudentStatusAndSemester/0/207')
            .expect(200);
        expect(response).toBeTruthy();
        expect(response.body.semester).toBe(0);
        expect(response.body.am).toBe(null);
    });

    it('should GET /eudoxus/getStudentStatusAndSemester/:studentInstituteIdentifier/:departmentCode', async () => {

        // set mapper
        app.get(ExpressDataApplication.name).useStrategy(StudentIdentifierMapper, StudentInstituteIdentifierMapper);
        app.get(ExpressDataApplication.name).useService(EudoxusService);
        // get an active student
        let student = await context.model('Student')
            .where('semester').equal(7)
            .or('semester').equal(8)
            .prepare()
            .and('studentStatus/alternateName').equal('active')
            .and('studentInstituteIdentifier').notEqual(null)
            .select('studentInstituteIdentifier', 'semester', 'department/alternativeCode as departmentCode')
            .silent()
            .getItem();
        expect(student).toBeTruthy();

        let response = await request(app)
            .get(`/eudoxus/getStudentStatusAndSemester/${student.studentInstituteIdentifier}/${student.departmentCode}`)
            .expect(200);
        expect(response).toBeTruthy();
        expect(response.body.semester).toBe(student.semester);
        expect(response.body.am).toBe(student.studentInstituteIdentifier);
        expect(response.body.status).toBe('active');

    });

    it('should GET /eudoxus/getStudentStatusAndSemester/:studentIdentifier/:departmentCode', async () => {

        app.get(ExpressDataApplication.name).useService(StudentIdentifierMapper);
        app.get(ExpressDataApplication.name).useService(EudoxusService);

        // get an active student
        let student = await context.model('Student')
            .where('semester').equal(7)
            .or('semester').equal(8)
            .prepare()
            .and('studentStatus/alternateName').equal('active')
            .select('studentIdentifier', 'semester', 'department/alternativeCode as departmentCode')
            .silent()
            .getItem();
        expect(student).toBeTruthy();

        let response = await request(app)
            .get(`/eudoxus/getStudentStatusAndSemester/${student.studentIdentifier}/${student.departmentCode}`)
            .expect(200);
        expect(response).toBeTruthy();
        expect(response.body.semester).toBe(student.semester);
        expect(response.body.am).toBe(student.studentIdentifier);
        expect(response.body.status).toBe('active');

        student = await context.model('Student')
            .where('studentStatus/alternateName').equal('graduated')
            .select('studentIdentifier', 'semester', 'department/alternativeCode as departmentCode')
            .silent()
            .getItem();
        expect(student).toBeTruthy();
        response = await request(app)
            .get(`/eudoxus/getStudentStatusAndSemester/${student.studentIdentifier}/${student.departmentCode}`)
            .expect(200);
        expect(response.body.semester).toBe(student.semester);
        expect(response.body.am).toBe(student.studentIdentifier);
        expect(response.body.status).toBe('inactive');

    });

    it('should GET /eudoxus/checkCourseRegistration/:studentIdentifier/:departmentCode/:academicYear/:academicPeriod', async () => {

        app.get(ExpressDataApplication.name).useService(StudentIdentifierMapper);
        app.get(ExpressDataApplication.name).useService(EudoxusService);

        // get an active student
        let student = await context.model('Student')
            .where('semester').equal(5)
            .or('semester').equal(6)
            .prepare()
            .and('studentStatus/alternateName').equal('active')
            .select('studentIdentifier', 'semester', 'department/alternativeCode as departmentCode')
            .silent()
            .getItem();
        expect(student).toBeTruthy();
        let response = await request(app)
            .get(`/eudoxus/checkCourseRegistration/${student.studentIdentifier}/${student.departmentCode}/2019/1`)
            .expect(200);
        expect(response.body).toBeTruthy();
        expect(response.body).toBeInstanceOf(Array);
        expect(response.body.length).toBe(0);

        // get student classes
        const studentRegistration = await context.model('StudentPeriodRegistration')
            .where('student/studentIdentifier').equal(student.studentIdentifier)
            .orderByDescending('registrationYear')
            .thenByDescending('registrationPeriod').silent().getItem();

        expect(studentRegistration).toBeTruthy();
        const studentClasses = await context.model('StudentCourseClass')
            .where('registration').equal(studentRegistration.id)
            .select('registration/registrationYear as registrationYear',
                'registration/registrationPeriod as registrationPeriod',
                'courseClass/course/displayCode as courseDisplayCode')
            .silent().getItems();
        expect(studentClasses).toBeInstanceOf(Array);
        expect(studentClasses.length).toBeGreaterThan(0);

        let courseCodes = studentClasses.map((studentClass) => {
            return new Buffer(studentClass.courseDisplayCode).toString('base64');
        }).join(',');
        response = await request(app)
            .get(`/eudoxus/checkCourseRegistration/${student.studentIdentifier}/${student.departmentCode}/${studentRegistration.registrationYear.id}/${studentRegistration.registrationPeriod.id}`)
            .query({
                courseCodes
            })
            .expect(200);
        expect(response.body).toBeTruthy();
        expect(response.body).toBeInstanceOf(Array);
        expect(response.body.length).toBe(studentClasses.length);
        studentClasses.forEach((studentClass) => {
           const find =  response.body.find((x) => {
              return x.courseCode === studentClass.courseDisplayCode
           });
           expect(find).toBeTruthy();
           expect(find.registration).toBe(true);
        });

        courseCodes = studentClasses.map((studentClass) => {
            return new Buffer(studentClass.courseDisplayCode + '-1').toString('base64');
        }).join(',');
        response = await request(app)
            .get(`/eudoxus/checkCourseRegistration/${student.studentIdentifier}/${student.departmentCode}/${studentRegistration.registrationYear.id}/${studentRegistration.registrationPeriod.id}`)
            .query({
                courseCodes
            })
            .expect(200);
        expect(response.body).toBeTruthy();
        expect(response.body).toBeInstanceOf(Array);
        expect(response.body.length).toBe(studentClasses.length);
        studentClasses.forEach((studentClass) => {
            const find =  response.body.find((x) => {
                return x.courseCode === studentClass.courseDisplayCode + '-1';
            });
            expect(find).toBeTruthy();
            expect(find.registration).toBe(false);
        });

    });


    it('should GET /eudoxus/checkCourseRegistration/:studentInstituteIdentifier/:departmentCode/:academicYear/:academicPeriod', async () => {


        app.get(ExpressDataApplication.name).useStrategy(StudentIdentifierMapper, StudentInstituteIdentifierMapper);
        app.get(ExpressDataApplication.name).useService(EudoxusService);

        // get an active student
        let student = await context.model('Student')
            .where('semester').equal(5)
            .or('semester').equal(6)
            .prepare()
            .and('studentStatus/alternateName').equal('active')
            .select('studentInstituteIdentifier',
                'semester',
                'department/alternativeCode as departmentCode'
            )
            .silent()
            .getItem();
        expect(student).toBeTruthy();
        let response = await request(app)
            .get(`/eudoxus/checkCourseRegistration/${student.studentInstituteIdentifier}/${student.departmentCode}/2019/1`)
            .expect(200);
        expect(response.body).toBeTruthy();
        expect(response.body).toBeInstanceOf(Array);
        expect(response.body.length).toBe(0);

        // get student classes
        const studentRegistration = await context.model('StudentPeriodRegistration')
            .where('student/studentInstituteIdentifier').equal(student.studentInstituteIdentifier)
            .orderByDescending('registrationYear')
            .thenByDescending('registrationPeriod').silent().getItem();

        expect(studentRegistration).toBeTruthy();
        const studentClasses = await context.model('StudentCourseClass')
            .where('registration').equal(studentRegistration.id)
            .select('registration/registrationYear as registrationYear',
                'registration/registrationPeriod as registrationPeriod',
                'courseClass/course/displayCode as courseDisplayCode')
            .silent().getItems();
        expect(studentClasses).toBeInstanceOf(Array);
        expect(studentClasses.length).toBeGreaterThan(0);

        let courseCodes = studentClasses.map((studentClass) => {
            return new Buffer(studentClass.courseDisplayCode).toString('base64');
        }).join(',');
        response = await request(app)
            .get(`/eudoxus/checkCourseRegistration/${student.studentInstituteIdentifier}/${student.departmentCode}/${studentRegistration.registrationYear.id}/${studentRegistration.registrationPeriod.id}`)
            .query({
                courseCodes
            })
            .expect(200);
        expect(response.body).toBeTruthy();
        expect(response.body).toBeInstanceOf(Array);
        expect(response.body.length).toBe(studentClasses.length);
        studentClasses.forEach((studentClass) => {
            const find =  response.body.find((x) => {
                return x.courseCode === studentClass.courseDisplayCode
            });
            expect(find).toBeTruthy();
            expect(find.registration).toBe(true);
        });

        courseCodes = studentClasses.map((studentClass) => {
            return new Buffer(studentClass.courseDisplayCode + '-1').toString('base64');
        }).join(',');
        response = await request(app)
            .get(`/eudoxus/checkCourseRegistration/${student.studentInstituteIdentifier}/${student.departmentCode}/${studentRegistration.registrationYear.id}/${studentRegistration.registrationPeriod.id}`)
            .query({
                courseCodes
            })
            .expect(200);
        expect(response.body).toBeTruthy();
        expect(response.body).toBeInstanceOf(Array);
        expect(response.body.length).toBe(studentClasses.length);
        studentClasses.forEach((studentClass) => {
            const find =  response.body.find((x) => {
                return x.courseCode === studentClass.courseDisplayCode + '-1';
            });
            expect(find).toBeTruthy();
            expect(find.registration).toBe(false);
        });

    });

});